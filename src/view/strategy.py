from re import compile
from weepy import Route
from sqlalchemy import and_
from pydantic import ValidationError

from lib.auth import Auth
from model.strategy import (
	Strategy as StrategyModel,
	StrategyVersion,
	StrategyValidator,
	StrategyValidatorCreate,
	StrategyVersionValidatorCreate
)
from lib.database import Database as db


class Strategy:
	@staticmethod
	@Route("/strategies", methods=["GET"])
	@Auth(right="strategy_read")
	async def strategies(req, resp, user):
		if "team_id" in req.headers:
			query = db.query(StrategyModel).filter(StrategyModel.team_id == req.headers["team_id"])
		else:
			query = db.query(StrategyModel).filter(StrategyModel.user_id == user.id)
		strategies = query.order_by(StrategyModel.id).all()
		rv = []
		for strategy in strategies:
			owner = strategy.user.nick if strategy.user_id else strategy.team.name
			versions = []
			for version in strategy.versions:
				versions.append("%s.%s.%s" % (version.major, version.minor, version.revision))
			rv.append({
				"id": strategy.id,
				"name": strategy.name,
				"versions": versions,
				"owner": owner,
				"public": strategy.public,
			})
		return {"status": True, "strategies": rv}

	@staticmethod
	@Route(compile(r"^/strategy/(\d+)/(\d+\.\d+\.\d+)/?$"), methods=["GET"])
	@Auth(right="strategy_read")
	async def strategy(req, resp, user, id, version):
		if strategy := Strategy.get_strategy(req, user, id):
			v = version.split(".")
			strategy_version = (
				db.query(StrategyVersion)
				.filter(StrategyVersion.strategy_id == id)
				.filter(
					and_(
						StrategyVersion.major == v[0],
						StrategyVersion.minor == v[1],
						StrategyVersion.revision == v[2]
					)
				)
				.one()
			)
			return {"status": True, "strategy": {
				"id": strategy.id,
				"name": strategy.name,
				"version": version,
				"code": strategy_version.code,
				"created": strategy_version.created,
				"public": strategy.public,
			}}
		return {"status": False, "code": "I404", "message": "Strategy does not exist"}

	staticmethod
	@Route("/strategy/create", methods=["POST"])
	@Auth(right="strategy_write")
	async def create(req, resp, user):
		try:
			data = StrategyValidatorCreate(**req.data["strategy"], **Strategy.get_owner(req, user))
		except ValidationError as e:
			return {"status": False, "code": "I103", "message": "Invalid input data: %s" % e}

		if db.query(StrategyModel).filter(StrategyModel.name == data.name).count():
			return {
				"status": False,
				"code": "I101",
				"message": "Strategy name already exist in your list"
			}
		strategy = StrategyModel.create(data)
		db.session.add(strategy)
		if "version" in req.data:
			db.session.flush()
			try:
				version = StrategyVersionValidatorCreate(
					strategy_id=strategy.id,
					created_by_id=user.id,
					**req.data["version"]
				)
			except ValidationError as e:
				db.session.rollback()
				return {"status": False, "code": "I103", "message": "Invalid input data: %s" % e}
			strategy_version = StrategyVersion.create(version)
			db.session.add(strategy_version)
		db.session.commit()
		return {"status": True}

	staticmethod
	@Route("/strategy/update", methods=["PUT"])
	@Auth(right="strategy_write")
	async def update(req, resp, user):
		try:
			data = StrategyValidator(**req.data["strategy"], **Strategy.get_owner(req, user))
		except ValidationError as e:
			return {"status": False, "code": "I103", "message": "Invalid input data: %s" % e}
		strategy = Strategy.get_strategy(req, user, data.id)
		if strategy and (strategy.public != data.public or strategy.name != data.name):
			strategy.update(data)
			version = [0, 0, 0]
			old_code = None
			for i in strategy.versions:
				if i.major >= version[0] and i.minor >= version[1] and i.revision >= version[2]:
					version = [i.major, i.minor, i.revision]
					old_code = i.code
			if "version" in req.data:
				iv = StrategyVersionValidatorCreate(
					strategy_id=strategy.id,
					created_by_id=user.id,
					**req.data["version"]
				)
				if old_code != iv.code:
					if (
						iv.major >= version[0] and
						iv.minor >= version[1] and
						iv.revision >= version[2] and
						version != [iv.major, iv.minor, iv.revision]
					):
						v = StrategyVersion.create(iv)
						db.session.add(v)
					else:
						return {"status": False, "code": "I104", "message": "Invalid version number"}
			db.session.commit()
			return {"status": True}
		return {"status": False, "code": "I404", "message": "Strategy does not exist"}

	staticmethod
	@Route(compile(r"^/strategy/delete/(\d+)/?$"), methods=["DELETE"])
	@Auth(right="service_write")
	async def delete(req, resp, user, id):
		if service := Strategy.get_strategy(req, user, id):
			db.session.query(StrategyVersion).filter_by(strategy_id=id).delete()
			db.session.delete(service)
			db.session.commit()
			return {"status": True}
		return {"status": False, "code": "B404", "message": "Bot not found"}

	@staticmethod
	def get_strategy(req, user, id):
		if "team_id" in req.headers:
			return (
				db.query(StrategyModel)
				.filter(StrategyModel.team_id == req.headers["team_id"])
				.filter(StrategyModel.id == id)
				.one_or_none()
			)
		else:
			return (
				db.query(StrategyModel)
				.filter(StrategyModel.user_id == user.id)
				.filter(StrategyModel.id == id)
				.one_or_none()
			)

	@staticmethod
	def get_owner(req, user):
		owner = {}
		if "team_id" in req.headers:
			owner["team_id"] = req.headers["team_id"]
		else:
			owner["user_id"] = user.id
		return owner
