from re import compile
from datetime import date
from weepy import Route
from sqlalchemy import and_, or_
from pydantic import ValidationError

from lib.auth import Auth
from model.test import (
	TestValidator,
	Test as TestModel,
	TestStrategyVersion,
	TestPair,
	TestFunds
)
from model.service import Service
from model.asset import Asset, Pair
from model.strategy import Strategy, StrategyVersion
from lib.database import Database as db


class Test:
	@staticmethod
	@Route("/tests", methods=["GET"])
	@Auth(right="test_read")
	async def tests(req, resp, user):
		if "team_id" in req.headers:
			query = db.query(TestModel).filter(TestModel.team_id == req.headers["team_id"])
		else:
			query = db.query(TestModel).filter(TestModel.user_id == user.id)
		tests = query.order_by(TestModel.id.desc()).all()
		rv = []
		for test in tests:
			rv.append({
				"id": test.id,
				"created": test.created,
				"finished": test.finished,
				"createdy_by_id": test.created_by_id,
				"service": test.service.name if test.service_id else None,
			})
		return {"status": True, "tests": rv}

	@staticmethod
	@Route("/test/options", methods=["GET"])
	@Auth(right="test_run")
	async def options(req, resp, user):
		options = {
			"services": {},
			"assets": {},
			"pairs": {},
			"strategies": {}
		}
		if "team_id" in req.headers:
			team_id = req.headers["team_id"]
			service_condition = Service.team_id == team_id
			strategy_condition = Strategy.team_id == team_id
		else:
			team_id = 0
			service_condition = Service.user_id == user.id
			strategy_condition = Strategy.user_id == user.id

		services = (
			db.query(Service)
			.filter_by(type=2)
			.filter(service_condition)
			.all()
		)
		for service in services:
			options["services"][str(service.id)] = service.name

		assets = db.query(Asset).all()
		for asset in assets:
			options["assets"][str(asset.id)] = asset.symbol

		pairs = db.query(Pair).all()
		for pair in pairs:
			options["pairs"][pair.symbol] = pair.id

		strategies = (
			db.query(Strategy)
			.filter(or_(strategy_condition, Strategy.public))
			.all()
		)
		for strategy in strategies:
			prefix = strategy.team.name if strategy.team_id else strategy.user.nick
			owned = strategy.team_id == team_id if team_id else strategy.user_id == user.id
			obj = {
				"name": strategy.name if owned else "%s/%s" % (prefix, strategy.name),
				"versions": {}
			}
			for version in strategy.versions:
				obj["versions"][str(version.id)] = "%s.%s.%s" % (version.major, version.minor, version.revision)
			options["strategies"][str(strategy.id)] = obj
		return {"status": True, "options": options}

	@staticmethod
	@Route("/test/run", methods=["POST"])
	@Auth(right="test_run")
	async def run(req, resp, user):
		try:
			if (
				"test" not in req.data or
				"strategies" not in req.data or
				"funds" not in req.data or
				type(req.data["test"]) != dict or
				type(req.data["strategies"]) != list or
				type(req.data["funds"]) != dict or
				not len(req.data["strategies"]) or not len(req.data["funds"].keys())
			):
				raise Exception("Invalid JSON data")

			dt = req.data["test"].copy()
			if "date_start" in dt:
				dt["date_start"] = date(*(int(i) for i in dt["date_start"].split("-")))
			if "date_stop" in dt:
				dt["date_stop"] = date(*(int(i) for i in dt["date_stop"].split("-")))
			if "date_start" in dt and "date_stop" in dt and dt["date_start"] > dt["date_stop"]:
				raise Exception("Invalid trading interval")

			data = TestValidator(
				**dt,
				**Test.get_owner(req, user),
				created_by_id=user.id
			)
		except (Exception, ValidationError) as e:
			return {"status": False, "code": "T101", "message": "Invalid input data: %s" % e}
		test = TestModel.create(data)
		db.session.add(test)
		db.session.flush()

		version_strategies = (
			db.query(StrategyVersion)
			.filter(StrategyVersion.id.in_(req.data["strategies"]))
			.all()
		)
		if len(version_strategies) != len(req.data["strategies"]):
			return {"status": False, "code": "T102", "message": "One or more strategy versions not found"}
		for i in req.data["strategies"]:
			db.session.add(TestStrategyVersion(test_id=test.id, strategy_version_id=i))

		if "pairs" in req.data:
			pairs = db.query(Pair).filter(Pair.id.in_(req.data["pairs"])).all()
			if len(pairs) != len(req.data["pairs"]):
				return {"status": False, "code": "T103", "message": "One or more pair not found"}
		else:
			pairs = db.query(Pair).all()
		ids = [pair.id for pair in pairs]
		for i in ids:
			db.session.add(TestPair(test_id=test.id, pair_id=i))

		amounts = req.data["funds"].values()
		for amount in amounts:
			if amount <= 0:
				return {
					"status": False,
					"code": "T105",
					"message": "Amounts contains 0 or negaitive values"
				}
		assets = db.query(Asset).filter(Asset.id.in_(req.data["funds"].keys())).all()
		if len(assets) != len(req.data["funds"].keys()):
			return {"status": False, "code": "T105", "message": "One or more asset not found"}
		for asset, amount in req.data["funds"].items():
			db.session.add(TestFunds(test_id=test.id, asset_id=asset, amount=amount))

		command = {
			"action": "run-test",
			"config": {
				"id": test.id,
				"strategies": {
					"%s_%s" % (v.strategy.owner.name, v.strategy.name): v.version
					for v in version_strategies
				},
				"pairs": [pair.symbol for pair in pairs],
				"funds": {a.symbol: req.data["funds"][str(a.id)] for a in assets}
			}
		}
		if "date_start" in req.data["test"]:
			command["config"]["date_start"] = req.data["test"]["date_start"]
		if "date_stop" in req.data["test"]:
			command["config"]["date_stop"] = req.data["test"]["date_stop"]

		print("command", command)

		db.session.commit()
		return {"status": True}

	@staticmethod
	def get_owner(req, user):
		owner = {}
		if "team_id" in req.headers:
			owner["team_id"] = req.headers["team_id"]
		else:
			owner["user_id"] = user.id
		return owner
