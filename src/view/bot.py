from re import compile
from weepy import Route
from sqlalchemy import and_
from pydantic import ValidationError

from lib.auth import Auth
from model.service import Service, ServiceValidatorCreate, ServiceValidator
from lib.database import Database as db


class Bot:
	@staticmethod
	@Route("/bots", methods=["GET"])
	@Auth(right="service_read")
	async def bots(req, resp, user):
		if "team_id" in req.headers:
			query = db.query(Service).filter(Service.team_id == req.headers["team_id"])
		else:
			query = db.query(Service).filter(Service.user_id == user.id)
		services = query.order_by(Service.id).all()
		bots = []
		for service in services:
			bots.append({
				"id": service.id,
				"name": service.name,
				"type": service.type,
				"state": service.state,
				"host": service.host,
				"port": service.port,
				"server_secret": service.server_secret
			})
		return {"status": True, "bots": bots}

	@staticmethod
	@Route(compile(r"^/bot/(\d+)/?$"), methods=["GET"])
	@Auth(right="service_read")
	async def bot(req, resp, user, id):
		if service := Bot.get_service(req, user, id):
			return {"status": True, "bot": {
				"id": service.id,
				"name": service.name,
				"type": service.type,
				"state": service.state,
				"host": service.host,
				"port": service.port,
				"server_secret": service.server_secret
			}}
		return {"status": False, "code": "B404", "message": "Bot not found"}

	staticmethod
	@Route("/bot/create", methods=["POST"])
	@Auth(right="service_write")
	async def create(req, resp, user):
		try:
			data = ServiceValidatorCreate(**req.data, **Bot.get_owner(req, user))
		except ValidationError as e:
			return {"status": False, "code": "B103", "message": "Invalid input data: %s" % e}

		if db.query(Service).filter(and_(Service.host == data.host, Service.port == data.port)).count():
			return {"status": False, "code": "B100", "message": "Bot already exist in your list"}
		if db.query(Service).filter(Service.name == data.name).count():
			return {"status": False, "code": "B101", "message": "Bot name already exist in your list"}
		bot = Service.create(data)
		db.session.add(bot)
		db.session.commit()
		return {"status": True}

	staticmethod
	@Route("/bot/update", methods=["PUT"])
	@Auth(right="service_write")
	async def update(req, resp, user):
		try:
			data = ServiceValidator(**req.data, **Bot.get_owner(req, user))
		except ValidationError as e:
			return {"status": False, "code": "B103", "message": "Invalid input data: %s" % e}
		if service := Bot.get_service(req, user, data.id):
			service.update(data)
			db.session.commit()
			return {"status": True}
		return {"status": False, "code": "B404", "message": "Bot does not exist"}

	staticmethod
	@Route(compile(r"^/bot/delete/(\d+)/?$"), methods=["DELETE"])
	@Auth(right="service_write")
	async def delete(req, resp, user, id):
		if service := Bot.get_service(req, user, id):
			db.session.delete(service)
			db.session.commit()
			return {"status": True}
		return {"status": False, "code": "B404", "message": "Bot not found"}

	@staticmethod
	def get_service(req, user, id):
		if "team_id" in req.headers:
			return (
				db.query(Service)
				.filter(Service.team_id == req.headers["team_id"])
				.filter(Service.id == id)
				.one_or_none()
			)
		else:
			return (
				db.query(Service)
				.filter(Service.user_id == user.id)
				.filter(Service.id == id)
				.one_or_none()
			)

	@staticmethod
	def get_owner(req, user):
		owner = {}
		if "team_id" in req.headers:
			owner["team_id"] = req.headers["team_id"]
		else:
			owner["user_id"] = user.id
		return owner
