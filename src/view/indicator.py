from re import compile
from weepy import Route
from sqlalchemy import and_
from pydantic import ValidationError

from lib.auth import Auth
from model.indicator import (
	Indicator as IndicatorModel,
	IndicatorVersion,
	IndicatorValidator,
	IndicatorValidatorCreate,
	IndicatorVersionValidatorCreate
)
from lib.database import Database as db


class Indicator:
	@staticmethod
	@Route("/indicators", methods=["GET"])
	@Auth(right="indicator_read")
	async def indicators(req, resp, user):
		if "team_id" in req.headers:
			query = db.query(IndicatorModel).filter(IndicatorModel.team_id == req.headers["team_id"])
		else:
			query = db.query(IndicatorModel).filter(IndicatorModel.user_id == user.id)
		indicators = query.order_by(IndicatorModel.id).all()
		rv = []
		for indicator in indicators:
			owner = indicator.user.nick if indicator.user_id else indicator.team.name
			versions = []
			for version in indicator.versions:
				versions.append("%s.%s.%s" % (version.major, version.minor, version.revision))
			rv.append({
				"id": indicator.id,
				"name": indicator.name,
				"versions": versions,
				"owner": owner,
				"public": indicator.public,
			})
		return {"status": True, "indicators": rv}

	@staticmethod
	@Route(compile(r"^/indicator/(\d+)/(\d+\.\d+\.\d+)/?$"), methods=["GET"])
	@Auth(right="indicator_read")
	async def indicator(req, resp, user, id, version):
		if indicator := Indicator.get_indicator(req, user, id):
			v = version.split(".")
			indicator_version = (
				db.query(IndicatorVersion)
				.filter(IndicatorVersion.indicator_id == id)
				.filter(
					and_(
						IndicatorVersion.major == v[0],
						IndicatorVersion.minor == v[1],
						IndicatorVersion.revision == v[2]
					)
				)
				.one()
			)
			return {"status": True, "indicator": {
				"id": indicator.id,
				"name": indicator.name,
				"version": version,
				"code": indicator_version.code,
				"created": indicator_version.created,
				"public": indicator.public,
			}}
		return {"status": False, "code": "I404", "message": "Indicator does not exist"}

	staticmethod
	@Route("/indicator/create", methods=["POST"])
	@Auth(right="indicator_write")
	async def create(req, resp, user):
		try:
			data = IndicatorValidatorCreate(**req.data["indicator"], **Indicator.get_owner(req, user))
		except ValidationError as e:
			return {"status": False, "code": "I103", "message": "Invalid input data: %s" % e}

		if db.query(IndicatorModel).filter(IndicatorModel.name == data.name).count():
			return {
				"status": False,
				"code": "I101",
				"message": "Indicator name already exist in your list"
			}
		indicator = IndicatorModel.create(data)
		db.session.add(indicator)
		if "version" in req.data:
			db.session.flush()
			try:
				version = IndicatorVersionValidatorCreate(
					indicator_id=indicator.id,
					created_by_id=user.id,
					**req.data["version"]
				)
			except ValidationError as e:
				db.session.rollback()
				return {"status": False, "code": "I103", "message": "Invalid input data: %s" % e}
			indicator_version = IndicatorVersion.create(version)
			db.session.add(indicator_version)
		db.session.commit()
		return {"status": True}

	staticmethod
	@Route("/indicator/update", methods=["PUT"])
	@Auth(right="indicator_write")
	async def update(req, resp, user):
		try:
			data = IndicatorValidator(**req.data["indicator"], **Indicator.get_owner(req, user))
		except ValidationError as e:
			return {"status": False, "code": "I103", "message": "Invalid input data: %s" % e}
		indicator = Indicator.get_indicator(req, user, data.id)
		if indicator and (indicator.public != data.public or indicator.name != data.name):
			indicator.update(data)
			version = [0, 0, 0]
			old_code = None
			for i in indicator.versions:
				if i.major >= version[0] and i.minor >= version[1] and i.revision >= version[2]:
					version = [i.major, i.minor, i.revision]
					old_code = i.code
			if "version" in req.data:
				iv = IndicatorVersionValidatorCreate(
					indicator_id=indicator.id,
					created_by_id=user.id,
					**req.data["version"]
				)
				if old_code != iv.code:
					if (
						iv.major >= version[0] and
						iv.minor >= version[1] and
						iv.revision >= version[2] and
						version != [iv.major, iv.minor, iv.revision]
					):
						v = IndicatorVersion.create(iv)
						db.session.add(v)
					else:
						return {"status": False, "code": "I104", "message": "Invalid version number"}
			db.session.commit()
			return {"status": True}
		return {"status": False, "code": "I404", "message": "Indicator does not exist"}

	staticmethod
	@Route(compile(r"^/indicator/delete/(\d+)/?$"), methods=["DELETE"])
	@Auth(right="service_write")
	async def delete(req, resp, user, id):
		if service := Indicator.get_indicator(req, user, id):
			db.session.query(IndicatorVersion).filter_by(indicator_id=id).delete()
			db.session.delete(service)
			db.session.commit()
			return {"status": True}
		return {"status": False, "code": "B404", "message": "Bot not found"}

	@staticmethod
	def get_indicator(req, user, id):
		if "team_id" in req.headers:
			return (
				db.query(IndicatorModel)
				.filter(IndicatorModel.team_id == req.headers["team_id"])
				.filter(IndicatorModel.id == id)
				.one_or_none()
			)
		else:
			return (
				db.query(IndicatorModel)
				.filter(IndicatorModel.user_id == user.id)
				.filter(IndicatorModel.id == id)
				.one_or_none()
			)

	@staticmethod
	def get_owner(req, user):
		owner = {}
		if "team_id" in req.headers:
			owner["team_id"] = req.headers["team_id"]
		else:
			owner["user_id"] = user.id
		return owner
