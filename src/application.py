from os.path import realpath
from logging import basicConfig, getLogger, INFO
from orjson import loads
from weepy import ASGI, Route
from lib.database import Database


if __name__ == 'application':
	__version__ = "__API_VERSION__"
	file = open(realpath(__file__).replace("src/application.py", "config/backend.json"), "rb")
	config = loads(file.read())
	file.close()

	# basicConfig()
	# getLogger('sqlalchemy.engine').setLevel(INFO)

	Database.init(
		config["db"]["user"],
		config["db"]["password"],
		config["db"]["host"],
		config["db"]["name"]
	)

	application = ASGI(allow="*")

	@Route("/")
	async def info(req, resp):
		return {
			"status": True,
			"version": __version__,
			"host": "%s://%s" % (req.scope["scheme"], ":".join((str(i) for i in req.scope["server"])))
		}

	from websocket import Websocket  # noqa
	from view.user import User  # noqa
	from view.bot import Bot  # noqa
	from view.indicator import Indicator  # noqa
	from view.strategy import Strategy  # noqa
	from view.test import Test  # noqa
