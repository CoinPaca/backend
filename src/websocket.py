from json import dumps
from weepy import Route
from lib.auth import Auth


class Websocket:
	_authorizations: dict = {}
	_users: dict = {}

	@staticmethod
	@Route("/socket", type="websocket")
	async def router(req, socket, *args, **kwargs):
		if req.event["type"] == "websocket.disconnect":
			Websocket.disconnect(req)
			return
		elif req.event["type"] == "websocket.receive":
			await Websocket.process(req, socket)

	@staticmethod
	async def process(req, socket):
		action = req.data.get("action")
		if Websocket.check_auth(req):
			data = req.data.get("data", {})
			# TODO: process data
		elif action == "auth":
			await socket.send(Websocket.authorize(req, socket))
		else:
			await socket.send({"status": False, "code": 1001, "message": "Forbidden"})

	@staticmethod
	async def send_to_user(user_id, message):
		if Websocket._users[user_id]:
			for socket in Websocket._users[user_id].items():
				socket.send(message if type(message) == str else dumps(message))

	@staticmethod
	def authorize(req, socket, **kwargs):
		data = req.data.get("data", {})
		code, result, user = Auth.check(
			data.get("id"),
			data.get("key"),
			data.get("device"),
			data.get("team_id", 0),
			right="service_command"
		)
		if code is True:
			id = "|".join(str(i) for i in req.scope["client"])
			Websocket._authorizations[id] = user.id
			if user.id not in Websocket._users:
				Websocket._users[user.id] = {}
			Websocket._users[user.id][id] = socket
			return {"status": True, "action": "auth"}
		return {**result, "status": False, "action": "auth"}

	@staticmethod
	def check_auth(req):
		return "|".join(str(i) for i in req.scope["client"]) in Websocket._authorizations

	@staticmethod
	def disconnect(req):
		id = "|".join(str(i) for i in req.scope["client"])
		user_id = Websocket._authorizations.pop(id, 0)
		if user_id in Websocket._users:
			Websocket._users[user_id].pop(id)
			if not Websocket._users[user_id].items():
				Websocket._users.pop(user_id)

