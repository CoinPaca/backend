from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base


class Role(Base):
	__tablename__ = "role"

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String, nullable=False, unique=True)

	rights = relationship("AccessRight", back_populates="roles", secondary="role_access_right")


class AccessRight(Base):
	__tablename__ = "access_right"

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String, nullable=False, unique=True)

	roles = relationship("Role", back_populates="rights", secondary="role_access_right")


class RoleAccessRight(Base):
	__tablename__ = "role_access_right"

	role_id = Column(Integer, ForeignKey("role.id"), nullable=False, primary_key=True)
	right_id = Column(Integer, ForeignKey("access_right.id"), nullable=False, primary_key=True)
