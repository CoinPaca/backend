from types import SimpleNamespace
from datetime import datetime, date
from sqlalchemy import (
	Column, Boolean, Integer, BigInteger, Float, Enum as EnumSQL, DateTime, Date,
	ForeignKey, CheckConstraint
)
from sqlalchemy.orm import relationship
from .base import Base, IntEnumSQL
from .position import Type, Status

from pydantic import BaseModel, ValidationError, validator
from typing import Optional


class TestValidator(BaseModel):
	service_id: int
	date_start: Optional[date] = None
	date_stop: Optional[date] = None
	user_id: Optional[int] = None
	team_id: Optional[int] = None
	created_by_id: int

	@validator("user_id")
	def team_or_user(cls, v, values):
		if not v and "team_id" not in values:
			raise ValidationError('One of field user_id or team_id must have a valid value')
		if v and v < 0:
			raise ValidationError('Invalid user id')
		return v

	@validator("user_id")
	def team_id_validity(cls, v, values):
		if v and v < 0:
			raise ValidationError('Invalid team id')
		return v


class Test(Base):
	__tablename__ = "test"
	__table_args__ = (
		CheckConstraint(
			"(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)",
			name="ck_test_user_or_team"
		),
		CheckConstraint(
			"(service_id IS NOT NULL) OR (finished IS NOT NULL)",
			name="ck_test_unfinished_test_service"
		)
	)

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	service_id = Column(Integer, ForeignKey("service.id"), nullable=True)
	date_start = Column(Date, nullable=True)
	date_stop = Column(Date, nullable=True)
	user_id = Column(Integer, ForeignKey('user.id'), nullable=True)
	team_id = Column(Integer, ForeignKey('team.id'), nullable=True)
	created = Column(DateTime, nullable=False, default=datetime.utcnow)
	finished = Column(DateTime, nullable=True)
	created_by_id = Column(Integer, ForeignKey("user.id"), nullable=False)

	service = relationship("Service")
	created_by = relationship("User", foreign_keys=[created_by_id])
	team = relationship('Team')
	user = relationship('User', foreign_keys=[user_id])

	@classmethod
	def create(cls, data: TestValidator):
		return cls(**{
			"service_id": data.service_id,
			"date_start": data.date_start,
			"date_stop": data.date_stop,
			"user_id": data.user_id,
			"team_id": data.team_id,
			"created_by_id": data.created_by_id
		})

	@property
	def owner(self):
		if self.user_id:
			return SimpleNamespace(type="user", id=self.user_id, name=self.user.nick)
		else:
			return SimpleNamespace(type="team", id=self.team_id, name=self.team.name)


class TestFunds(Base):
	__tablename__ = "test_funds"

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	test_id = Column(BigInteger, ForeignKey("test.id"))
	asset_id = Column(Integer, ForeignKey("asset.id"))
	amount = Column(Float, nullable=False)
	locked = Column(Boolean, nullable=False, default=False)

	test = relationship("Test")
	asset = relationship("Asset")


class TestPair(Base):
	__tablename__ = "test_pair"
	test_id = Column(BigInteger, ForeignKey("test.id"), primary_key=True)
	pair_id = Column(BigInteger, ForeignKey("pair.id"), primary_key=True)

	test = relationship("Test")
	pair = relationship("Pair")


class TestPosition(Base):
	__tablename__ = "test_position"

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	type = Column(EnumSQL(Type), nullable=False)
	test_id = Column(BigInteger, ForeignKey("test.id"), nullable=True)
	pair_id = Column(Integer, ForeignKey("pair.id"), nullable=False)
	funds_id = Column(BigInteger, ForeignKey("test_funds.id"), nullable=False)
	status = Column(IntEnumSQL(Status), nullable=False)
	open_amount = Column(Float, nullable=False)
	close_amount = Column(Float, nullable=True)
	open_price = Column(Float, nullable=False)
	close_price = Column(Float, nullable=True)
	open_time = Column(DateTime, nullable=False)
	close_time = Column(DateTime, nullable=True)
	profit = Column(Float, nullable=True)
	strategy_version_id = Column(BigInteger, ForeignKey("strategy_version.id"), nullable=False)
	order_id = Column(BigInteger, nullable=True)

	test = relationship("Test")
	pair = relationship("Pair")
	funds = relationship("TestFunds")
	strategy_version = relationship("StrategyVersion")


class TestStrategyVersion(Base):
	__tablename__ = "test_strategy_version"

	test_id = Column(BigInteger, ForeignKey("test.id"), primary_key=True)
	strategy_version_id = Column(BigInteger, ForeignKey("strategy_version.id"), primary_key=True)

	test = relationship("Test")
	strategy_version = relationship("StrategyVersion")
