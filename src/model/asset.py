from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship
from .base import Base


class Asset(Base):
	__tablename__ = 'asset'
	__table_args__ = (
		UniqueConstraint("symbol", "name", name="uq_asset_symbol"),
	)

	id = Column(Integer, primary_key=True, autoincrement=True)
	symbol = Column(String, nullable=False)
	name = Column(String, nullable=False)
	image_ext = Column(String, nullable=True)
	coingecko_id = Column(String, nullable=False)


class Pair(Base):
	__tablename__ = 'pair'
	__table_args__ = (
		UniqueConstraint("base_asset_id", "quote_asset_id", name="uq_pair_base_asset_id"),
	)

	id = Column(Integer, primary_key=True, autoincrement=True)
	symbol = Column(String, nullable=False)
	base_asset_id = Column(Integer, ForeignKey("asset.id"), nullable=False)
	quote_asset_id = Column(Integer, ForeignKey("asset.id"), nullable=True)

	base_asset = relationship('Asset', foreign_keys=[base_asset_id])
	quote_asset = relationship('Asset', foreign_keys=[quote_asset_id])
