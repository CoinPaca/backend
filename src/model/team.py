from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from .base import Base


class Team(Base):
	__tablename__ = "team"

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(String, nullable=False, unique=True)


class TeamUser(Base):
	__tablename__ = "team_user"

	team_id = Column(Integer, ForeignKey('team.id'), primary_key=True, index=True)
	user_id = Column(Integer, ForeignKey('user.id'), primary_key=True, index=True)
	role_id = Column(Integer, ForeignKey('role.id'), nullable=False)

	team = relationship('Team')
	user = relationship('User')
	role = relationship('Role')
