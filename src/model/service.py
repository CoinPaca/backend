from types import SimpleNamespace
from enum import IntEnum
from sqlalchemy import (
	Column, Integer, String, ForeignKey,
	CheckConstraint, UniqueConstraint
)
from sqlalchemy.orm import relationship
from .base import Base, IntEnumSQL

from pydantic import BaseModel, Field, ValidationError, validator
from typing import Optional


class State(IntEnum):
	RUNNING = 0
	STOPPED = 1
	DISCONNECTED = 2


class ServiceType(IntEnum):
	TRADER = 0
	NOTIFIER = 1
	TESTER = 2


class ServiceValidatorCreate(BaseModel):
	type: ServiceType
	state: Optional[State]
	name: str
	host: str
	port: int = Field(..., ge=0, lt=65536)
	server_secret: str
	user_id: Optional[int] = None
	team_id: Optional[int] = None

	@validator("user_id")
	def team_or_user(cls, v, values):
		if not v and "team_id" not in values:
			raise ValidationError('One of field user_id or team_id must have a valid value')
		if v and v < 0:
			raise ValidationError('Invalid user id')
		return v

	@validator("user_id")
	def team_id_validity(cls, v, values):
		if v and v < 0:
			raise ValidationError('Invalid team id')
		return v


class ServiceValidator(ServiceValidatorCreate):
	id: int = Field(..., gt=0)


class Service(Base):
	__tablename__ = "service"
	__table_args__ = (
		UniqueConstraint("name", "user_id", "team_id", name="uq_service_name"),
		UniqueConstraint("host", "port", name="uq_service_host"),
		CheckConstraint(
			"(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)",
			name="ck_service_user_or_team"
		)
	)

	id = Column(Integer, primary_key=True, autoincrement=True)
	name = Column(
		String,
		nullable=False
	)
	state = Column(IntEnumSQL(State), nullable=False)
	type = Column(IntEnumSQL(ServiceType), nullable=False)
	host = Column(String, nullable=False)
	port = Column(Integer, nullable=False)
	server_secret = Column(String, nullable=True)
	user_id = Column(Integer, ForeignKey('user.id'), nullable=True)
	team_id = Column(Integer, ForeignKey('team.id'), nullable=True)

	team = relationship('Team')
	user = relationship('User')

	def update(self, data: ServiceValidator):
		self.type = data.type
		self.name = data.name
		self.host = data.host
		self.port = data.port
		self.server_secret = data.server_secret
		self.user_id = data.user_id
		self.team_id = data.team_id

	@classmethod
	def create(cls, data: ServiceValidatorCreate):
		instance = cls(**{
			"name": data.name,
			"state": 2,
			"type": data.type,
			"host": data.host,
			"port": data.port,
			"server_secret": data.server_secret
		})
		if data.team_id:
			instance.team_id = data.team_id
		else:
			instance.user_id = data.user_id
		return instance

	@property
	def owner(self):
		if self.user_id:
			return SimpleNamespace(type="user", id=self.user_id, name=self.user.nick)
		else:
			return SimpleNamespace(type="team", id=self.team_id, name=self.team.name)
