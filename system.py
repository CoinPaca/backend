#!/usr/bin/python3
import sys
from re import compile
from os import path
from random import choice
from string import ascii_letters, digits
from argparse import ArgumentParser
try:
	from ujson import loads, dumps
except ImportError:
	from json import loads, dumps
from getpass import getpass
from bcrypt import hashpw, gensalt
from alembic.config import Config
from alembic import command
from lib.database import Database
from model import *  # noqa
from model.user import User
from model.role import Role, AccessRight, RoleAccessRight
import psycopg2


def check_any(value, regs):
	for reg in regs:
		if reg.match(value):
			return True
	return False


def required(caption, pattern=None, any=[]):
	i = None
	regs = []
	if pattern:
		any.append(pattern)
	regs = [compile(pattern or ".*") for pattern in any]
	while not i or (not check_any(i, regs) and len(regs)):
		if i is not None and not len(i):
			print("This information is required!")
		elif i and pattern:
			print("Invalid format!")
		print(caption)
		i = input()
	return i


def configure():
	file = open(path.realpath(__file__).replace("/system.py", "/config/backend.json"), "w")

	hosts = [
		r"^(((?!-))(xn--|_{1,1})?[a-z0-9-]{0,61}[a-z0-9]{1,1}\.)*(xn--)?([a-z0-9][a-z0-9\-]{0,60}|[a-z0-9-]{1,30}\.[a-z]{2,})$",  # noqa domain
		r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$",  # ipv4
		r"(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"  # noqa ipv6
	]

	config = {}
	config["debug"] = False
	config["host"] = required(
		"Enter API IP or domain:",
		any=hosts
	)
	config["port"] = int(required(
		"Enter API port number:",
		pattern=r"^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$"
	))
	print("Enter SSL certificate path (keep empty for disable SSL):")
	config["ssl"] = input() or False

	config["db"] = {}
	print("Enter database host and port (default localhost:5432):")
	config["db"]["host"] = input() or "localhost:5432"
	print("Enter database user (default postgres):")
	config["db"]["user"] = input() or "postgres"
	print("Enter database password (default no password):")
	config["db"]["password"] = input()
	config["db"]["name"] = required(
		"Enter database name:",
		pattern=r"^[a-zA-Z0-9_]+$",
		any=[]
	)

	config["email"] = {}
	config["email"]["smtp_server"] = required(
		"Enter SMTP server: ", any=hosts
	)
	config["email"]["smtp_port"] = required(
		"Enter SMTP port: ",
		pattern=r"^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$",
		any=[]
	)
	config["email"]["address"] = required(
		"Enter email address: ",
		pattern=r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b",
		any=[]
	)
	config["email"]["password"] = required("Enter email password: ", pattern=None, any=[])
	config["secret"] = ''.join(choice(ascii_letters + digits) for i in range(32))

	file.write(dumps(config, indent=2))
	file.close()
	return config


def install(config, args):
	if not config:
		config = configure()

	file = open(path.realpath(__file__).replace("/system.py", "/alembic.ini"), "r+")
	txt = file.read()
	file.seek(0)
	file.write(txt.replace("__DB_ADDR__", "postgresql+psycopg2://%s:%s@%s/%s" % (
		args.db_user[0] or config["db"]["user"],
		args.db_password[0] or config["db"]["password"],
		config["db"]["host"],
		config["db"]["name"],
	)))
	file.close()

	if not args.skip_schema:
		db = Database(
			args.db_user[0] or config["db"]["user"],
			args.db_password[0] or config["db"]["password"],
			config["db"]["host"],
			""
		)
		query = "SELECT count(datname) FROM pg_database WHERE datname='%s'" % config["db"]["name"]
		if db.engine.execute(query).fetchone()[0] == 0:
			host, port = config["db"]["host"].split(":")
			conn = psycopg2.connect(
				database="postgres",
				user=args.db_user[0] or config["db"]["user"],
				password=args.db_password[0] or config["db"]["password"],
				host=host,
				port=port
			)
			conn.autocommit = True
			cursor = conn.cursor()
			cursor.execute("CREATE DATABASE %s WITH ENCODING 'UTF8'" % config["db"]["name"])
			conn.close()

		db = Database(
			args.db_user[0] or config["db"]["user"],
			args.db_password[0] or config["db"]["password"],
			config["db"]["host"],
			config["db"]["name"]
		)
		metadata = base.Base.metadata  # noqa
		metadata.create_all(db.engine)
		alembic_cfg = Config(path.realpath(__file__).replace("/system.py", "/alembic.ini"))
		command.stamp(alembic_cfg, "head")
	email = required(
		"Enter superuser email:",
		pattern=r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
	)
	nick = required("Enter superuser nick (cannot contain @):", pattern=r"^[^@]+$", any=[])
	print("Enter superuser password (at least 6 characters):")
	password = getpass()
	print("Enter superuser password again:")
	password_again = getpass()
	while password != password_again or len(password) < 6:
		print("INCORRECT!")
		print("Enter superuser password (at least 6 characters):")
		password = getpass()
		print("Enter superuser password again:")
		password_again = getpass()
	hash = hashpw(password.encode(), gensalt()).decode()
	user = User(
		nick=nick,
		email=email,
		password=hash
	)
	maintainer = Role(name="maintainer")
	db.session.add(maintainer)
	db.session.add(user)
	db.session.commit()
	user_list = AccessRight(name="user_list")
	user_management = AccessRight(name="user_management")
	role_management = AccessRight(name="role_management")
	team_management = AccessRight(name="team_management")
	service_read = AccessRight(name="service_read")
	service_write = AccessRight(name="service_write")
	service_notify = AccessRight(name="service_notify")
	service_command = AccessRight(name="service_command")
	indicator_read = AccessRight(name="indicator_read")
	indicator_write = AccessRight(name="indicator_write")
	strategy_read = AccessRight(name="strategy_read")
	strategy_write = AccessRight(name="strategy_write")
	test_read = AccessRight(name="test_read")
	test_write = AccessRight(name="test_write")
	test_run = AccessRight(name="test_run")
	db.session.add(user_list)
	db.session.add(user_management)
	db.session.add(role_management)
	db.session.add(team_management)
	db.session.add(service_read)
	db.session.add(service_write)
	db.session.add(service_notify)
	db.session.add(service_command)
	db.session.add(test_read)
	db.session.add(test_write)
	db.session.add(test_run)
	db.session.add(indicator_read)
	db.session.add(indicator_write)
	db.session.add(strategy_read)
	db.session.add(strategy_write)

	admin = Role(name="admin")
	trader = Role(name="trader")
	analyst = Role(name="analyst")
	db.session.add(admin)
	db.session.add(trader)
	db.session.add(analyst)

	db.session.commit()

	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=user_list.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=user_management.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=role_management.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=team_management.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=service_read.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=service_write.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=service_notify.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=service_command.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=test_read.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=test_write.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=test_run.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=indicator_read.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=indicator_write.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=strategy_read.id))
	db.session.add(RoleAccessRight(role_id=maintainer.id, right_id=strategy_write.id))

	db.session.add(RoleAccessRight(role_id=admin.id, right_id=user_list.id))
	db.session.add(RoleAccessRight(role_id=admin.id, right_id=user_management.id))
	db.session.add(RoleAccessRight(role_id=admin.id, right_id=role_management.id))
	db.session.add(RoleAccessRight(role_id=admin.id, right_id=team_management.id))
	db.session.add(RoleAccessRight(role_id=admin.id, right_id=service_read.id))
	db.session.add(RoleAccessRight(role_id=admin.id, right_id=service_write.id))

	db.session.add(RoleAccessRight(role_id=trader.id, right_id=service_read.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=service_notify.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=service_command.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=test_read.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=test_write.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=test_run.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=indicator_read.id))
	db.session.add(RoleAccessRight(role_id=trader.id, right_id=strategy_read.id))

	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=service_read.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=service_notify.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=test_read.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=test_write.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=test_run.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=indicator_read.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=indicator_write.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=strategy_read.id))
	db.session.add(RoleAccessRight(role_id=analyst.id, right_id=strategy_write.id))

	db.session.commit()


def update(config):
	Database.init(
		config["db"]["user"],
		config["db"]["password"],
		config["db"]["host"],
		config["db"]["name"]
	)
	alembic_cfg = Config(path.realpath(__file__).replace("/system.py", "/alembic.ini"))
	command.upgrade(alembic_cfg, 'head')


def generate(message):
	alembic_cfg = Config(path.realpath(__file__).replace("/system.py", "/alembic.ini"))
	command.revision(alembic_cfg, message=message, autogenerate=True)


if __name__ == "__main__":
	parser = ArgumentParser(description="BotAPI - CryptoBOT communication interface")
	parser.add_argument('--install', action="store_true")
	parser.add_argument('--generate', action="store_true")
	parser.add_argument('--update', action="store_true")
	parser.add_argument('--skip-schema', action="store_true")
	parser.add_argument('--message', nargs=1, default=[""])
	parser.add_argument('--db-user', nargs=1, default=[""])
	parser.add_argument('--db-password', nargs=1, default=[""])
	args = parser.parse_args()
	sys.path.insert(0, path.realpath(__file__).replace("/system.py", "/src"))

	try:
		file = open(path.realpath(__file__).replace("/system.py", "/config/backend.json"), "r")
		config = loads(file.read())
		file.close()
	except Exception:
		config = None

	if args.generate:
		generate(args.message[0] or None)
	elif args.update:
		update(config)
	elif args.install:
		install(config, args)
